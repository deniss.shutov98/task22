import "./styles/styles.css";
import "./styles/elementstyles.css";
import "./styles/textstyles.css";
import breakfast1 from "./images/breakfast1.jpg";
import breakfast2 from "./images/breakfast2.jpg";
import breakfast3 from "./images/breakfast3.jpg";
import lunch1 from "./images/lunch1.jpg";
import lunch2 from "./images/lunch2.jpg";
import lunch3 from "./images/lunch3.jpg";
import shakes1 from "./images/shakes1.jpg";
import shakes2 from "./images/shakes2.jpg";
import shakes3 from "./images/shakes3.jpg";
import dinner1 from "./images/dinner1.jpg";
import dinner2 from "./images/dinner2.jpg";
import dinner3 from "./images/dinner3.jpg";

assignVisibilityByCategory("all");

function assignVisibilityByCategory(category) {
  const menuItems = document.getElementsByClassName("menu-item");
  if (category === "all") {
    category = "";
  }

  for (const menuItem of menuItems) {
    menuItem.classList.remove("show");
    if (menuItem.className.indexOf(category) > -1) {
      menuItem.classList.add("show");
    }
  }
}

// Add active class to the current button (highlight it)
const btnContainer = document.getElementById("btnContainer");
const btns = btnContainer.getElementsByClassName("btn");

for (const btn of btns) {
  btn.addEventListener("click", function () {
    const current = document.getElementsByClassName("active");
    current[0].className = current[0].className.replace(" active", "");
    this.className += " active";
    assignVisibilityByCategory(this.id);
  });
}

// Add images to img tags
document.getElementById("breakfast1").src = breakfast1;
document.getElementById("breakfast2").src = breakfast2;
document.getElementById("breakfast3").src = breakfast3;
document.getElementById("lunch1").src = lunch1;
document.getElementById("lunch2").src = lunch2;
document.getElementById("lunch3").src = lunch3;
document.getElementById("shakes1").src = shakes1;
document.getElementById("shakes2").src = shakes2;
document.getElementById("shakes3").src = shakes3;
document.getElementById("dinner1").src = dinner1;
document.getElementById("dinner2").src = dinner2;
document.getElementById("dinner3").src = dinner3;
